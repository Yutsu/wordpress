<?php

// BEGIN iThemes Security - Ne modifiez pas ou ne supprimez pas cette ligne
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Désactivez l’éditeur de code - iThemes Security > Réglages > Ajustements WordPress > Éditeur de code
// END iThemes Security - Ne modifiez pas ou ne supprimez pas cette ligne

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'SWv: K2F$s(3s6e*eKgW!uE~$}Gfi#d-nxWMSF^ud1}WVMVDyA+VOxt5VU.pQ^*|' );
define( 'SECURE_AUTH_KEY',  'w[:/IVcNIhH24&R@RF[KU>D&weTA{Opw>^{48_kY6ndk v^c5|ag[P^b?q.A7lk4' );
define( 'LOGGED_IN_KEY',    'C;yxC/H}k*T}:6SL8B~a5}mL:ZSG#)iP#<Yzza!_M$2oY6%mh#e]qjtS>R-$WBLc' );
define( 'NONCE_KEY',        ':1):m&H$%Hi-ttt29g##9N,B-TgyR3w72KjX(C?V&dwLyDoZK R5T[EGwI5Zgc5A' );
define( 'AUTH_SALT',        '~x-kj0n>djbW4N-D/MgI.F>I|T+.3|mM-in?Wffz cPEE{RmvVw &@^pX!fO-keV' );
define( 'SECURE_AUTH_SALT', ':eJ*-l=*=<->5o;,~-XVz@^-NBJf?Wzd-Npliu|viKw(f?=6.Osb2iW%)(NjRkeF' );
define( 'LOGGED_IN_SALT',   'LZ65JocYc%QsTO %Eoo-z)gefA/d#)yzPY ? F*<LeISYQX&|M;rZieE;U)`|iLh' );
define( 'NONCE_SALT',       ')#drltd<(K/ldQCD2J]c|xc}a2]C,?v{mnym8z>q1JfYl+XNopCo=46MY.bkvl>E' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wordpress_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
