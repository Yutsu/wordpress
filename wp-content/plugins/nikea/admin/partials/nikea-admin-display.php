<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       kipdev.io
 * @since      1.0.0
 *
 * @package    Nikea
 * @subpackage Nikea/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
hello
<!-- <?php print_r($dokkyDocuments); ?> -->

<form class="dokky-modal__form">
  <input type="hidden" id="authorId" value="<?php echo get_current_user_id(); ?>">

  <input type="hidden" id="documentId" value="0" />
  <div class="dokky-modal__form__field">
    <label>
      <?php _e('Document title', 'Dokky'); ?> *
      <input type="text" maxlength="120" id="documentTitle" />
    </label>
  </div>
  <div class="dokky-modal__form_field">
    <label>
      <?php _e('Document description', 'Dokky'); ?>
      <textarea id="documentDescription" cols="20" rows="2"></textarea>
    </label>
  </div>

  <div class="dokky-modal__form_field" id="addFileField">
    
    <button class="dokky-modal__form__field__choose-file>" id="dokky-media-open" type="button">
      <?php _e('Choose file') ?>
    </button>
    <input type="hidden" name="dokky_file_id" id="dokky_file_id" value="">
    <p id="fileChoosed"></p>
  </div>

  <div>
    <button class="dokky-modal__form__field__submit" id="buttonAddDocument" type="button" onclick="addDocument()">Submit</button>
  </div>
</form>

<!-- Faire un foreach de dokkyDocuments -->