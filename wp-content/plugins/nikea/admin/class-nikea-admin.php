<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       kipdev.io
 * @since      1.0.0
 *
 * @package    Nikea
 * @subpackage Nikea/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nikea
 * @subpackage Nikea/admin
 * @author     Edwina Tan <edwina.tan@hotmail.fr>
 */
class Nikea_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nikea_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nikea_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nikea-admin.css', array(), $this->version, 'all' );

	}

	public function register_admin_menu() {
		add_menu_page(
			__('Dokky', 'textdomain'),
			'Dokky',
			'manage_options',
			'dokky',
			array($this, 'admin_dokky_index'),
			'dashicons-media-document',
			6
		);
	}

	public function admin_dokky_index(){
		$dokkyTags = $this->getDokkyTags();
		$dokkyDocuments = $this->getDokkyDocuments();
		// $nbPage = round($countDokkyDocuments/$resultPerPage);
		include('partials/nikea-admin-display.php');
	}

	public function getDokkyTags(){
		global $wpdb;
		$dokkyTags = $wpdb->get_results(
			$wpdb->prepare("SELECT * FROM {$wpdb->prefix}dokky_tags")
		);
		return $dokkyTags;
	}

	public function getDokkyDocuments(){
		global $wpdb;
		$dokkyDocuments = $wpdb->get_results(
			$wpdb->prepare("SELECT * FROM {$wpdb->prefix}dokky_documents")
		);
		return $dokkyDocuments;
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nikea_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nikea_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nikea-admin.js', array( 'jquery' ), $this->version, false );

	}

}
