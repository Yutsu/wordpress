<?php

/**
 * Fired during plugin deactivation
 *
 * @link       kipdev.io
 * @since      1.0.0
 *
 * @package    Nikea
 * @subpackage Nikea/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Nikea
 * @subpackage Nikea/includes
 * @author     Edwina Tan <edwina.tan@hotmail.fr>
 */
class Nikea_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
