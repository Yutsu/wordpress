<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              kipdev.io
 * @since             1.0.0
 * @package           Nikea
 *
 * @wordpress-plugin
 * Plugin Name:       Nikea
 * Plugin URI:        google.fr
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Edwina Tan
 * Author URI:        kipdev.io
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nikea
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'NIKEA_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-nikea-activator.php
 */
function activate_nikea() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nikea-activator.php';
	$activator = new Nikea_Activator;
	$activator->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nikea-deactivator.php
 */
function deactivate_nikea() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nikea-deactivator.php';
	Nikea_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nikea' );
register_deactivation_hook( __FILE__, 'deactivate_nikea' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nikea.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_nikea() {

	$plugin = new Nikea();
	$plugin->run();

}
run_nikea();
